from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Bintang Ilham Syahputra' # TODO Implement this
birth_year = 1998

# Create your views here.
def index(request):
    response = {'name': mhs_name,'years': calculate_age(birth_year)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    response = date.today().year-birth_year
    return response
    pass
